<?php

/*
 * Fresns (https://fresns.org)
 * Copyright (C) 2021-Present Jevan Tang
 * Released under the Apache-2.0 License.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Fresns Engine Tips Language Lines
    |--------------------------------------------------------------------------
    */

    'errorKey' => '服务异常，密钥不存在',

    'errorApi' => '服务异常，获取不到完整的 API 配置信息。',
    'errorDesktopTheme' => '配置异常，获取不到电脑端主题模板。',
    'errorMobileTheme' => '配置异常，获取不到手机端主题模板。',

    'settingApiTip' => '设置位置: 管理后台 -> 客户端 -> 网站网页 -> 网站引擎配置',
    'settingThemeTip' => '设置位置: 管理后台 -> 应用中心 -> 网站引擎 -> 关联主题模板',
];
