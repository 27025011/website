<?php

/*
 * Fresns (https://fresns.org)
 * Copyright (C) 2021-Present Jevan Tang
 * Released under the Apache-2.0 License.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Fresns Engine Tips Language Lines
    |--------------------------------------------------------------------------
    */

    'errorKey' => 'Service exception, key does not exist',

    'errorApi' => 'Service exception, can\'t get the complete API configuration information.',
    'errorDesktopTheme' => 'Configuration exception, can\'t get the theme template on computer side.',
    'errorMobileTheme' => 'Configuration exception, can\'t get the theme template for cell phone.',

    'settingApiTip' => 'Set Path: Admin Panel -> Client -> Website -> Engine Config',
    'settingThemeTip' => 'Set Path: Admin Panel -> App Center -> Engines -> Theme Config',
];
