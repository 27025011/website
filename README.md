<p align="center"><a href="https://fresns.cn" target="_blank"><img src="https://cdn.fresns.cn/images/logo.png" width="300"></a></p>

<p align="center">
<img src="https://img.shields.io/badge/Platform-Web-blue" alt="Platform">
<img src="https://img.shields.io/badge/PHP-%5E8.1-blueviolet" alt="PHP">
<img src="https://img.shields.io/badge/Fresns-2.x-orange" alt="Fresns">
<img src="https://img.shields.io/badge/License-Apache--2.0-green" alt="License">
</p>

## 介绍

Fresns 是一款免费开源的社交网络服务软件，专为跨平台而打造的通用型社区产品，支持灵活多样的内容形态，可以满足多种运营场景，符合时代潮流，更开放且更易于二次开发。

- [点击了解产品 16 个功能特色](https://docs.fresns.cn/guide/features.html)
- 使用者请阅读[安装教程](https://docs.fresns.cn/guide/install.html)和[运营文档](https://docs.fresns.cn/operating/)；
- 扩展插件开发者请阅读[扩展文档](https://docs.fresns.cn/extensions/)和[数据字典](https://docs.fresns.cn/database/)；
- 客户端开发者（网站端、小程序、App）请阅读 [API 文档](https://docs.fresns.cn/api/)。

## 仓库介绍

Fresns 官方开发的网站引擎，集成在主程序中运行。

### 引擎安装

- 使用标识名安装: `FresnsEngine`
- 使用指令安装: `php artisan market:require FresnsEngine`

**主题模板开发**

- [主题功能](https://docs.fresns.cn/extensions/theme/functions.html)
- [路径结构](https://docs.fresns.cn/extensions/theme/structure.html)
- [模板标签](https://docs.fresns.cn/extensions/theme/tags.html)

## 使用说明

遵循 [Fresns 客户端设计理念](https://docs.fresns.cn/extensions/idea.html#%E5%AE%A2%E6%88%B7%E7%AB%AF)，引擎以结构化方式实现了全部功能，使用者可以根据自己的需求，自定义页面风格、交互体验、栏目命名、入口路径等，实现各自个性化的运营场景。

- 开发者可基于插件指令安装 [https://docs.fresns.cn/extensions/plugin/artisan.html](https://docs.fresns.cn/extensions/plugin/artisan.html)
- 使用者请在 Fresns 应用市场下载安装 [https://marketplace.fresns.cn](https://marketplace.fresns.cn)

## 加入我们

Fresns 的开源社区正在急速增长中，如果你认可我们的开源软件，有兴趣为 Fresns 的发展做贡献，竭诚欢迎[加入我们](https://docs.fresns.cn/community/join.html)一起开发完善。无论是[报告错误](https://docs.fresns.cn/guide/feedback.html)或是 Pull Request 开发，那怕是修改一个错别字也是对我们莫大的帮助。

贡献指南：[https://docs.fresns.cn/contributing/](https://docs.fresns.cn/contributing/)

## 许可协议

Fresns 是根据 [Apache-2.0](https://opensource.org/licenses/Apache-2.0) 授权的开源软件。
